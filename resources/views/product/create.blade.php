<form method="POST" action="/" class="form-group">
  {{ csrf_field() }}
  <div class="form-group">
    <label for="product_name">Product name</label>
    <input type="text" class="form-control" name="name" id="product_name" placeholder="Product Name">
  </div>
  <div class="form-group">
    <label for="stock">Quantity in stock</label>
    <input type="text" class="form-control" name="stock" id="stock" placeholder="Quantity in stock">
  </div>
  <div class="form-group">
    <label for="price">Price per item</label>
    <input type="text" class="form-control" name="price" id="price" placeholder="Price per item">
  </div>
  <button type="submit" class="btn btn-default">Add product</button>
</form>
