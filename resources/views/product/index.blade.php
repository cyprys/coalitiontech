@extends('_main')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Product page</h1>
        </div>
        <div class="row">
          @if (count($products))
            <table class="table">
              <tr>
                <th>Product name</th>
                <th>In stock</th>
                <th>Price</th>
              </tr>
            @foreach($products as $product)
              <tr>
                <td>{{ $product->name }}</td>
                <td>{{ $product->stock }}</td>
                <td>{{ $product->price * $product->stock }}</td>
              </tr>
            @endforeach
            </table>
          @endif



        </div>
        <button id="add_product" class="btn btn-primary">Add product</button>
    </div>

@stop
@section('footer_scripts')
<div class="modal fade" id="product_add_modal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add product</h4>
      </div>
      <div class="modal-body" id="add_proudct_content">

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
$(document).delegate('#add_product','click', function(){

  $.ajax({
        url: '/create',
        dataType: 'html',
        async: false,
        beforeSend: function() {
            $('#add_product').button('loading');
        },
        complete: function() {
            $('#add_product').button('reset');
        },
        success: function(html) {
          $('#add_proudct_content').html(html);
          $('#product_add_modal').modal('show');
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

</script>
@stop
